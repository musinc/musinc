# MUSINC

A simple music player to sync and play through all your devices.

## What is MUSINC?

Musinc is a simple music player, for people who needs to store their music in a local 
storage(Phone, Laptop, Smart TV etc..) and sync those devices together to play the required music in any desired
device by using the other synced device as a remote control at the same time.

## How does it work (My plan:))?

![alt text](https://gitlab.com/musinc/musinc/-/blob/main/images/musinc1_.jpg?raw=true)

As seen in the above representation the process is as follows:
1. Setup a server and start the Musinc_server.py programm.
2. Setup a private musinc account by entering the required username and password.

4. Using the muinc software, type in your username and load your songs into it to add a special meta tag to your 
   music files. ie; These metadata consists of your user id and a specific tag for the song, this is how the server 
   can communicate between your devices without transferring the music itself but just the tag and command.
5. Save the newly updated songs onto all your devices. (IF required: You can now delete the intially saved songs as the
   updated songs are just the normal song with just an added metadata and can be played in any of your normal device 
   as usuall)
6. Connect to your musinc server and login to your account in all your devices.
7. Sync and play your music online! 

## Current Status

-Program to add the metadata in progress (working on the interface for bulk song tagging)!!
